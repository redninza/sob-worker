<?php
$gmc= new GearmanClient();

# add the default job server
$gmc->addServer('127.0.0.1', '4730');

# add another task, but this one to run in the background
$task = $gmc->addTaskHigh("SendIncompleteProfileMail", json_encode(array('status' => "inactive")));

if (! $gmc->runTasks())
{
    echo "ERROR " . $gmc->error() . "\n";
    exit;
}
