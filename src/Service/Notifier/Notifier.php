<?php

namespace Service\Notifier;

class Notifier
{
    protected $pusher;

    public function __construct($key, $secret, $appId, $debug = true)
    {
        $this->pusher = new \Pusher($key, $secret, $appId, $debug);
    }

    public function trigger($channel, $event, $data)
    {
        return $this->pusher->trigger($channel, $event, $data, null, true, false);
    }

    public function socketAuth($channel, $socketId, $customData = false)
    {
        return $this->pusher->socket_auth($channel, $socketId, $customData);
    }

    public function presenceAuth($channel, $socketId, $userId, $customData = false)
    {
        return $this->pusher->presence_auth($channel, $socketId, $userId, $customData);
    }
}