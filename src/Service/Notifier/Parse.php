<?php

namespace Service\Notifier;

use Parse\ParseClient;
use Parse\ParseInstallation;
use Parse\ParsePush;

class Parse
{
    protected $parse;

    public function __construct($appId, $restKey, $masterKey, $curlDebug = true)
    {
        ParseClient::initialize($appId, $restKey, $masterKey, $curlDebug);
    }

    public function trigger ($userId, $data, $target = 'channel')
    {
        $response = array();

        switch ($target) {
            case 'channel'  :   $response = ParsePush::send(
                                    array(
                                        "channels" => ["channel-$userId"],
                                        "data"     => $data,
                                    )
                                );
                                break;

            case 'user'     :   $query = ParseInstallation::query();
                                $query->equalTo('userId', "user-$userId");

                                $response = ParsePush::send(
                                    array(
                                        "where" => $query,
                                        "data"  => $data,
                                    )
                                );
                                break;

            default:            break;
        }

        return $response;
    }
}