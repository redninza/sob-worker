<?php

namespace Service\HttpClient;

class Factory
{
    public static function get($client)
    {
        switch ($client) {
            case 'HttpRequest':
                return new HttpRequest();
        }
    }
}