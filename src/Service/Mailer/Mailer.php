<?php

namespace Service\Mailer;

use SendGrid\Email;
use SendGrid\Web;

class Mailer
{
    /**
     * @var Email
     */
    public $mailer;

    /**
     * @var Web
     */
    public $web;

    public function __construct($conf)
    {
        $this->mailer = new Email();
        $this->web = new Web($conf['mail']['username'], $conf['mail']['password']);

    }

    public function send()
    {
        return $this->web->send($this->mailer);
    }
}