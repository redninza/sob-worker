<?php

namespace Model;

class Percentile
{
    public function getPercentileSpanCalculate ($finalScore, $span)
    {
        $division = 100 / $span;
        $calculateSpan = (int) ((floor($finalScore * $division)) * $division);

        if ($calculateSpan > 100) {
            $calculateSpan = 100;
        }

        return $calculateSpan;
    }
}