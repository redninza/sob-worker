<?php

namespace Model;

class Matching
{
    /** @var \stdClass */
    protected $job;

    /** @var \stdClass */
    protected $seeker;

    /** @var array */
    protected $scores;

    /** @var \stdClass */
    protected $settings;

    /** @var float */
    protected $salaryRangeExtension = 1.2;

    /** @var float */
    protected $nonMatchConstant = 0.8;

    /** @var array */
    protected $conf;

    /** @var float */
    protected $locationScore;

    public function __construct(\stdClass $job, \stdClass $seeker, $conf)
    {
        $this->job = $job;
        $this->seeker = $seeker;
        $this->settings = $this->job->company->company_settings;
        $this->conf = $conf;
    }

    public function getScores($location_matching_preference = '')
    {
        $totalMatchScore = 0;

        $skillScore = 0;

        if (!empty($this->job->skills)) {
            if (!empty($this->seeker->skills)) {
                echo 'Getting skill score.' . PHP_EOL;
                $skillScore = $this->getSkillScore();
                $skillScore = ($skillScore > 1) ? 1 : $skillScore;
            }
        } else {
            if (!empty($this->seeker->skills)) {
                echo 'Setting skill score = 0.8 as no skill is required in job.' . PHP_EOL;
                $skillScore = $this->nonMatchConstant;
            }
        }
        echo "skill score: " . $skillScore . PHP_EOL;
        echo PHP_EOL;

        $experienceScore = 0;

        if (!empty($this->seeker->experiences)) {
            echo 'Getting experience score.' . PHP_EOL;
            $experienceScore = $this->getExperienceScore();
            $experienceScore = ($experienceScore > 1) ? 1 : $experienceScore;
        }
        echo "Experience Score: " . $experienceScore . PHP_EOL;
        echo PHP_EOL;

        $educationScore = 0;

        if (!empty($this->job->education)) {
            if (!empty($this->seeker->educations)) {
                echo 'Getting education score.' . PHP_EOL;
                $educationScore = $this->getEducationScore();
                $educationScore = ($educationScore > 1) ? 1 : $educationScore;
            }
        } else {
            if (!empty($this->seeker->educations)) {
                echo 'Setting education score = 0 as no education found for seeker.' . PHP_EOL;
                $educationScore = $this->nonMatchConstant;
            }
        }
        echo "education score: " . $educationScore . PHP_EOL;
        echo PHP_EOL;

        $languageScore = 0;

        if (!empty($this->job->languages)) {
            if (!empty($this->seeker->language)) {
                echo 'Getting languages score.' . PHP_EOL;
                $languageScore = $this->getLanguageScore();
                $languageScore = ($languageScore > 1) ? 1 : $languageScore;
            }
        } else {
            if (!empty($this->seeker->language)) {
                echo 'Setting languages score = 0 as no languages found for seeker.' . PHP_EOL;
                $languageScore = $this->nonMatchConstant;
            }
        }
        echo "language score: " . $languageScore . PHP_EOL;
        echo PHP_EOL;

        $expectationScore = 0;

        if (!empty($this->job->expectation)) {
            if (!empty($this->seeker->expectation)) {
                echo 'Getting expectation score.' . PHP_EOL;
                $expectationScore = $this->getExpectationScore();
                $expectationScore = ($expectationScore > 1) ? 1 : $expectationScore;
            }
        } else {
            if (!empty($this->seeker->expectation)) {
                echo 'Setting expectation score = 0.8 as no expectation is required in job.' . PHP_EOL;
                $expectationScore = $this->nonMatchConstant;
            }
        }
        echo PHP_EOL;


        // algorithm v2 rev 4

        $educationWeight   = $this->conf['matchingweight']['education'] / $this->conf['matchingweight']['total'];
        $languageWeight    = $this->conf['matchingweight']['language'] / $this->conf['matchingweight']['total'];
        $expectationWeight = $this->conf['matchingweight']['expectation'] / $this->conf['matchingweight']['total'];
        $skillWeight       = $this->conf['matchingweight']['skill'] / $this->conf['matchingweight']['total'];
        $experienceWeight  = $this->conf['matchingweight']['experience'] / $this->conf['matchingweight']['total'];


        echo "Weights from configs (skillweight, experienceweight, educationweight, langweight, expectationweight):::
                $skillWeight, $experienceWeight, $educationWeight, $languageWeight, $expectationWeight" . PHP_EOL;


        $skillScore       = (($skillScore * $skillWeight) > 1) ? 1 : ($skillScore * $skillWeight);
        $experienceScore  = (($experienceScore * $experienceWeight) > 1) ? 1 : ($experienceScore * $experienceWeight);
        $educationScore   = (($educationScore * $educationWeight) > 1) ? 1 : ($educationScore * $educationWeight);
        $languageScore    = (($languageScore * $languageWeight) > 1) ? 1 : ($languageScore * $languageWeight);
        $expectationScore = (($expectationScore * $expectationWeight) > 1) ? 1 : ($expectationScore * $expectationWeight);


        $totalMinScore = ($skillScore + $educationScore + $languageScore + $expectationScore) / 5;

        echo "is job experience missing? :::";var_dump($this->jobExperienceMissing());echo PHP_EOL;

        echo "total min score ::: $totalMinScore" . PHP_EOL;

        echo "min score without exp from config ::: " . $this->conf['min_score_without_exp'] . PHP_EOL;

        if ($this->jobExperienceMissing() && ($totalMinScore < $this->conf['min_score_without_exp'])) {

            $skillWeight      = $this->conf['matchingweight']['fresherSkill'] / $this->conf['matchingweight']['total'];
            $experienceWeight = $this->conf['matchingweight']['fresherExperience'] / $this->conf['matchingweight']['total'];

            echo "Weights while job experience is not required and totalMinScore is less than " . $this->conf['min_score_without_exp'] .
                 "(skillweight, experienceweight)::: $skillWeight, $experienceWeight" . PHP_EOL;

            $skillScore       = (($skillScore * $skillWeight) > 1) ? 1 : ($skillScore * $skillWeight);
            $experienceScore  = (($experienceScore * $experienceWeight) > 1) ? 1 : ($experienceScore * $experienceWeight);

        }

        $totalMatchScore = ($skillScore + $educationScore + $languageScore + $expectationScore + $experienceScore) / 5;

        if($totalMatchScore > 1) {

            $totalMatchScore = 1;
        }

        echo "Individual scores (skill, experience, education, lang, expectation)::: $skillScore, $experienceScore, $educationScore, $languageScore, $expectationScore" . PHP_EOL;


        if ((($experienceScore + $skillScore) < $this->conf['min_score_with_exp_and_skill']) && ($this->locationScore > 0)) {

            $totalMatchScore = $totalMatchScore / 4;

        } elseif ((!$this->jobExperienceMissing()) && ($experienceScore < $this->conf['min_score_with_exp']) && (($experienceScore + $skillScore) < $this->conf['min_score_with_exp_and_skill_with_exp'])) {

            $totalMatchScore = $totalMatchScore / 3;
        }


        if ($this->locationScore == 0) {

            $totalMatchScore = $totalMatchScore / 4;

        }

        echo "final score: " . $totalMatchScore . PHP_EOL;

        $this->scores = array(
            'skill_score'       => $skillScore,
            'experience_score'  => $experienceScore,
            'education_score'   => $educationScore,
            'language_score'    => $languageScore,
            'expectation_score' => $expectationScore,
            'final_score'       => $totalMatchScore
        );

        return $this->scores;
    }

    private function getWeightedAverage(array $elements, $n = null)
    {
        $total = 0;

        foreach ($elements as $value => $weight) {
            $score = $value * $weight;
            $total += ($score > 1) ? 1 : $score;
        }

        if (is_null($n)) {
            $n = count($elements);
        }

        return $total / $n;
    }

    public function getSkillScore()
    {
        // Total number of skills
        $N = 0;

        // Over-qualification factor
        $oQ = !empty($this->settings->over_qualification_factor_for_skill) ? ($this->settings->over_qualification_factor_for_skill) : 0;

        // Final skill score
        $S = 0;

        // Default individual skill score
        $s = 0;

        $seekerSkills = $this->seeker->skills;
        $jobSkills    = $this->job->skills;

        if (!empty($jobSkills)) {

            foreach($jobSkills as $jobSkill) {

                if (!empty($jobSkill->keywords) || !empty($jobSkill->category) || !empty($jobSkill->duration) || !empty($jobSkill->level)) {

                    $N++;

                    if (!empty($seekerSkills)) {

                        foreach($seekerSkills as $seekerSkill) {

                            if (strtolower($jobSkill->keywords) == strtolower($seekerSkill->keywords)) {

                                $Ls = $seekerSkill->level;

                                if (empty($Ls)) {
                                    $Ls = 0;
                                }

                                $Lp = $jobSkill->level;

                                if (empty($Lp)) {
                                    $Lp = 0;
                                }

                                if ($Lp > 0) {
                                    $Li = ($Ls / $Lp);
                                } else {
                                    $Li = 0;
                                }

                                if ($Li > 1.0) {
                                    $L = 1.0 + ($oQ * ($Li - 1.0));
                                } else {
                                    $L = $Li;
                                }

                                $Ds = $seekerSkill->years;

                                if (!isset($Ds) || empty($Ds)) {
                                    $Ds = 0;
                                }

                                $Dp = $jobSkill->years;

                                if (empty($Dp)) {
                                    $Dp = 0;
                                }

                                if ($Dp > 0) {
                                    $Di = ($Ds / $Dp);
                                } else {
                                    $Di = 1;
                                }

                                if ($Di > 1.0) {
                                    $D = 1.0 + ($oQ * ($Di - 1.0));
                                } else {
                                    $D = $Di;
                                }

                                $s += ($L * $D);
                            }

                        }

                    }

                }

            }

        }

        if ($N > 0) {
            $S = ($s / $N);
        }

        return $S;
    }

    // TODO: Clearly implement the Age factor
    public function getExperienceScore()
    {
        // Total number of job experience
        $N = 0;

        // Initial value
        $finalCCValues[$N] = 0.0;

        // Over-qualification factor
        $oQ = !empty($this->settings->over_qualification_factor_for_experience) ? ($this->settings->over_qualification_factor_for_experience) : 0;

        $seekerExperiences = $this->seeker->experiences;

        $jobExperience = array(
            'soc' => $this->job->soc,
            'duration' => $this->job->duration
        );

        $finalDuration = array();

        if (isset($seekerExperiences)) {
            foreach ($seekerExperiences as $seekerExperience) {
                if ($seekerExperience->soc != "") {
                    $Ds = (float) $seekerExperience->duration;
                    $seekerSoc = $seekerExperience->soc;

                    echo "Seeker Experience SOC is set: " . $seekerSoc .PHP_EOL;
                    echo "Seeker Experience Duration is set: " . $Ds .PHP_EOL;

                    $seekerSocLevel1 = substr($seekerSoc, 0, 2);
                    $seekerSocLevel2 = substr($seekerSoc, 3, 2);
                    $seekerSocLevel3 = substr($seekerSoc, 5, 1);
                    $seekerSocLevel4 = substr($seekerSoc, 6, 1);
                    $seekerSocTitle  = $seekerExperience->designation;

                    $Dp = (float) $jobExperience['duration'];

                    if (!empty($jobExperience['soc'])) {
                        $providerSoc = $jobExperience['soc'];
                        $providerSocLevel1 = substr($providerSoc, 0, 2);
                        $providerSocLevel2 = substr($providerSoc, 3, 2);
                        $providerSocLevel3 = substr($providerSoc, 5, 1);
                        $providerSocLevel4 = substr($providerSoc, 6, 1);

                        if ($providerSocLevel1 == $seekerSocLevel1) {
                            $finalCCValues[$N] = 0.1;
                            if ($providerSocLevel2 == $seekerSocLevel2) {
                                $finalCCValues[$N] = 0.3;
                                if ($providerSocLevel3 == $seekerSocLevel3) {
                                    $finalCCValues[$N] = 0.6;
                                    if ($providerSocLevel4 == $seekerSocLevel4) {
                                        $finalCCValues[$N] = 1.0;
                                    }
                                }
                            }
                        }
                    } elseif (!empty($this->job->experience_custom_soc) && !empty($this->job->experience_custom_soc->title)) {

                        $providerSoc = $this->job->experience_custom_soc;
                        $providerSocLevel1 = $providerSoc->soc1;
                        $providerSocLevel2 = $providerSoc->soc2;
                        $providerSocLevel3 = ($providerSoc->soc3) / 10;
                        $providerSocTitle  = $providerSoc->title;

                        if (empty($providerSocLevel1) || empty($providerSocLevel2) || empty($providerSocLevel3)) {
                            $finalCCValues[$N] = 1.0;

                        } else {

                            if ($providerSocLevel1 == $seekerSocLevel1) {
                                $finalCCValues[$N] = 0.1;
                                if ($providerSocLevel2 == $seekerSocLevel2) {
                                    $finalCCValues[$N] = 0.3;
                                    if ($providerSocLevel3 == $seekerSocLevel3) {
                                        $finalCCValues[$N] = 0.6;
                                        if (md5($providerSocTitle) == md5($seekerSocTitle)) {
                                            $finalCCValues[$N] = 1.0;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $finalCCValues[$N] = 1.0;
                    }

                    if ($Dp > 0) {
                        $Di = ($Ds / $Dp);
                    } else {
                        $Di = 1;
                    }

                    if ($Di > 1.0) {
                        $D = 1.0 + ($oQ * ($Di - 1.0));
                    } else {
                        $D = $Di;
                    }

                    $finalDuration[$N] = $D;

                    $N++;

                } else {
                    echo "Seeker experience custom soc is set" . PHP_EOL;

                    $Ds = (float) $seekerExperience->duration;
                    $seekerSoc = $seekerExperience->experience_custom_soc;

                    if (!empty($seekerExperience->experience_custom_soc) && !empty($seekerExperience->experience_custom_soc->title)) {
                        $seekerSocLevel1 = $seekerSoc->soc1;
                        $seekerSocLevel2 = $seekerSoc->soc2;
                        $seekerSocLevel3 = ($seekerSoc->soc3) / 10;
                        $seekerSocTitle  = $seekerExperience->designation;

                    } else {
                        $seekerSocLevel1 = '';
                        $seekerSocLevel2 = '';
                        $seekerSocLevel3 = '';
                        $seekerSocTitle = '';
                    }

                    $Dp = (float) $jobExperience['duration'];

                    if (!empty($jobExperience['soc'])){
                        $providerSoc = $jobExperience['soc'];
                        $providerSocLevel1 = substr($providerSoc, 0, 2);
                        $providerSocLevel2 = substr($providerSoc, 3, 2);
                        $providerSocLevel3 = substr($providerSoc, 5, 1);
                        $providerSocTitle  = $this->job->experience_title;

                        if ($providerSocLevel1 == $seekerSocLevel1) {
                            $finalCCValues[$N] = 0.1;
                            if ($providerSocLevel2 == $seekerSocLevel2) {
                                $finalCCValues[$N] = 0.3;
                                if ($providerSocLevel3 == $seekerSocLevel3) {
                                    $finalCCValues[$N] = 0.6;
                                    if (md5($providerSocTitle) == md5($seekerSocTitle)) {
                                        $finalCCValues[$N] = 1.0;
                                    }
                                }
                            }
                        }
                    } elseif (!empty($this->job->experience_custom_soc) && !empty($this->job->experience_custom_soc->title)) {

                        $providerSoc = $this->job->experience_custom_soc;
                        $providerSocLevel1 = $providerSoc->soc1;
                        $providerSocLevel2 = $providerSoc->soc2;
                        $providerSocLevel3 = ($providerSoc->soc3) / 10;
                        $providerSocTitle  = $providerSoc->title;

                        if (empty($providerSocLevel1) || empty($providerSocLevel2) || empty($providerSocLevel3)) {
                            $finalCCValues[$N] = 1.0;

                        } else {

                            if ($providerSocLevel1 == $seekerSocLevel1) {
                                $finalCCValues[$N] = 0.1;
                                if ($providerSocLevel2 == $seekerSocLevel2) {
                                    $finalCCValues[$N] = 0.3;
                                    if ($providerSocLevel3 == $seekerSocLevel3) {
                                        $finalCCValues[$N] = 0.6;
                                        if (md5($providerSocTitle) == md5($seekerSocTitle)) {
                                            $finalCCValues[$N] = 1.0;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $finalCCValues[$N] = 1.0;
                    }

                    if ($Dp > 0) {
                        $Di = ($Ds / $Dp);
                    } else {
                        $Di = 1;
                    }

                    if ($Di > 1.0) {
                        $D = 1.0 + ($oQ * ($Di - 1.0));
                    } else {
                        $D = $Di;
                    }

                    $finalDuration[$N] = $D;

                    $N++;
                }

            }

        }

        echo "CC array: <pre>";
        print_r($finalCCValues);
        echo "</pre>Duration array: <pre>";
        print_r($finalDuration);

        if (count($finalCCValues) > 0) {
            $CC = max($finalCCValues);
        } else {
            $CC = 0;
        }

        $maxDurationValues = 1;
        foreach($finalCCValues as $key=>$value){
            if ($value == $CC){
                $maxDurationValues = $finalDuration[$key];
            }
        }

        echo "final CC value: $CC" . PHP_EOL;
        echo "final Duration value: $maxDurationValues" . PHP_EOL;

        $experienceScore = ($CC * $maxDurationValues);

        return $experienceScore;
    }

    public function getEducationScore()
    {
        $seekerEducationData = $this->seeker->educations;
        $jobEducationData = $this->job->education;

        $educationScores = array();

        // Over-Qualification Factor
        $oQ = !empty($this->settings->over_qualification_factor_for_education) ? ($this->settings->over_qualification_factor_for_education) : 0;

        if (isset($seekerEducationData) && !empty($seekerEducationData)) {
            foreach($seekerEducationData as $key => $value) {

                $Ls = (int) $value->degree;

                if (!isset($Ls) || empty($Ls)) {
                    $Ls = 0;
                }

                $Lp = $jobEducationData->degree;

                if (!isset($Lp) || empty($Lp)) {
                    $Lp = 0;
                }

                if ($Lp > 0) {
                    $L = ($Ls / $Lp);
                    echo "Education Level score : ". $L . PHP_EOL;

                    echo "calculating over qualification factor ...." . $oQ . PHP_EOL;
                    if ($L > 1.0) {
                        $L = 1.0 + ($oQ * ($L - 1.0));
                    }
                } else {
                    $L = 0.0;
                }

                $F = 1.0;

                echo "Education Level score after calculating over qualification factor : ". $L . PHP_EOL;
                echo "Education field of study...." . $jobEducationData->field_of_study . PHP_EOL;

                if (isset($jobEducationData->field_of_study) && !empty($jobEducationData->field_of_study)) {
                    if (isset($value->field_of_study) && !empty($value->field_of_study)) {
                        if (strtolower(trim($jobEducationData->field_of_study)) == strtolower(trim($value->field_of_study))) {
                            $F = 1.0;
                        } else {
                            $F = 0.0;
                        }
                    } else {
                        $F = 0.0;
                    }
                }

                echo "Score from field of study : " . $F . PHP_EOL;

                $score = ($L + $F) / 2;
                if ($score > 1.0) {
                    $score = 1.0 + ($oQ * ($score - 1.0));
                }

                echo "Average education score: " . $score . PHP_EOL;

                $educationScores[] = $score;
            }
        }

        $educationScore = max($educationScores);

        echo "maximum and final education score: " . $educationScore . PHP_EOL;

        return $educationScore;
    }

    public function getLanguageScore()
    {
        $seekerLanguages = $this->seeker->language;
        $jobLanguages = $this->job->languages;

        // Over-qualification factor
        $oQ = !empty($this->settings->over_qualification_factor_for_language) ? ($this->settings->over_qualification_factor_for_language) : 0;

        // Final Language score
        $LS = 0;

        // Total number of languages
        $N = 0;

        // Total language score
        $L = 0;

        if (isset($jobLanguages) && !empty($jobLanguages)) {

            foreach($jobLanguages as $jobLanguage) {

                $N++;

                if (isset($seekerLanguages) && !empty($seekerLanguages)) {

                    foreach($seekerLanguages as $seekerLanguage) {

                        if (($jobLanguage->name != "") && (strtolower($jobLanguage->name) == strtolower($seekerLanguage->name))) {

                            $LSs = $this->reverseLanguageLevel($seekerLanguage->speaking_level);

                            if (!isset($LSs) || empty($LSs)) {
                                $LSs = 0;
                            }

                            $LSp = $this->reverseLanguageLevel($jobLanguage->speaking_level);

                            if (!isset($LSp) || empty($LSp)) {
                                $LSp = 0;
                            }

                            if ($LSp > 0) {
                                $LSi = ($LSs / $LSp);
                            } else {
                                $LSi = 0;
                            }

                            $LRs = $this->reverseLanguageLevel($seekerLanguage->reading_level);

                            if (!isset($LRs) || empty($LRs)) {
                                $LRs = 0;
                            }

                            $LRp = $this->reverseLanguageLevel($jobLanguage->reading_level);

                            if (!isset($LRp) || empty($LRp)) {
                                $LRp = 0;
                            }

                            if ($LRp > 0) {
                                $LRi = ($LRs / $LRp);
                            } else {
                                $LRi = 0;
                            }

                            $LWs = $this->reverseLanguageLevel($seekerLanguage->writing_level);

                            if (!isset($LWs) || empty($LWs)) {
                                $LWs = 0;
                            }

                            $LWp = $this->reverseLanguageLevel($jobLanguage->writing_level);

                            if (!isset($LWp) || empty($LWp)) {
                                $LWp = 0;
                            }

                            if ($LWp > 0) {
                                $LWi = ($LWs / $LWp);
                            } else {
                                $LWi = 0;
                            }

                            $Li = ($LSi + $LRi + $LWi) / 3;

                            if ($Li > 1.0) {
                                $LTi = 1.0 + ($oQ * ($Li - 1.0));
                            } else {
                                $LTi = $Li;
                            }

                            $L += $LTi;

                        } else if (($seekerLanguage->name == "")) {

                            $L += 0;

                        }

                    }

                }

            }

        }

        if ($N > 0) {
            $LS = ($L / $N);
        }

        return $LS;
    }

    public function getExpectationScore()
    {
        // get salary score
        $salaryScore = $this->getSalaryScore();

        //get location score
        $locationScore = $this->getRevisedLocationScore();

        $expectationScore = ($salaryScore + $locationScore) / 2;

        echo "expectation score => " . $expectationScore . PHP_EOL;

        return $expectationScore;
    }

    public function getSalaryScore($location_matching_preference = '')
    {
        $minSs = (int) $this->seeker->expectation->salary_start;
        $maxSp = (int) $this->job->expectation->salary_end;
        $SRE = $this->salaryRangeExtension;

        if (!isset($minSs) || empty($minSs)) {
            $minSs = 0;
        }

        if (!isset($maxSp) || empty($maxSp)) {
            $maxSp = 0;
        }

        if ($minSs == 0 && $maxSp > 0) {
            //return $this->nonMatchConstant;
            echo "Seeker salary score => 1 as seeker salary input is 0" . PHP_EOL;
            return 1;
        }

        switch ($this->seeker->expectation->payment_type) {
            case 'hourly': $minSs = $minSs * 40 * 52; break;
            case 'weekly': $minSs = $minSs * 52; break;
            case 'monthly': $minSs = $minSs * 12; break;
        }

        switch ($this->job->expectation->payment_type) {
            case 'hourly': $maxSp = $maxSp * 40 * 52; break;
            case 'weekly': $maxSp = $maxSp * 52; break;
            case 'monthly': $maxSp = $maxSp * 12; break;
        }

        // G = Gap between seeker's minimum salary and job posting's maximum salary
        $G = $minSs - $maxSp;

        // SRE = Proposed salary range extension value (20%)
        $ER = $minSs * $SRE;

        if (($G != 0) && ($minSs <= $maxSp)) {
            $S = 1.0;
        } elseif (($ER - $G) > 0) {
            $S = ($ER - $G) / $ER;
        } else {
            $S = 0;
        }

        echo "Seeker salary score =>" . $S . PHP_EOL;

        return $S;
    }

    public function getLocationScore($location_matching_preference = '')
    {
        if (empty($this->seeker->addresses)){
            return 0;
        }

        if (empty($this->job->location)){
            return 0;
        }

        $seekerLocations = $this->seeker->addresses;
        $jobLocation = $this->job->location;

        $countryScore = 0;
        $stateScore = 0;
        $cityScore = 0;
        $postalCodeScore = 0;

        $seekerCountry = '';
        $seekerCity = '';
        $seekerState = '';
        $seekerPostalCode = '';

        $jobCountry = '';
        $jobCity = '';
        $jobState = '';
        $jobPostalCode = '';

        $seekerRelocation = '';

        $finalScore = 0.0;

        // seeker and job country
        if(!empty($seekerLocations[0]->country)) {
            $seekerCountry = strtolower($this->formatVocabulary($seekerLocations[0]->country));
        }

        if(!empty($jobLocation->country)) {
            $jobCountry = strtolower($this->formatVocabulary($jobLocation->country));
        }

        // seeker and job state
        if(!empty($seekerLocations[0]->state)) {
            $seekerState = strtolower($this->formatVocabulary($seekerLocations[0]->state));
        }

        if(!empty($jobLocation->state)) {
            $jobState = strtolower($this->formatVocabulary($jobLocation->state));
        }

        // seeker and job city
        if(!empty($seekerLocations[0]->city)) {
            $seekerCity = strtolower($seekerLocations[0]->city);
        }

        if(!empty($jobLocation->city)) {
            $jobCity = strtolower($jobLocation->city);
        }

        // seeker and job postal code
        if(!empty($seekerLocations[0]->postal_code)) {
            $seekerPostalCode = strtolower($seekerLocations[0]->postal_code);
        }

        if(!empty($jobLocation->postal_code)) {
            $jobPostalCode = strtolower($jobLocation->postal_code);
        }

        if(!empty($this->seeker->expectation->relocation)) {
            $seekerRelocation = strtolower($this->seeker->expectation->relocation);
        }

        if(empty($seekerRelocation) || strtolower($seekerRelocation) == 'no-relocation') {

            if($seekerPostalCode == $jobPostalCode) {
                $postalCodeScore = .25;
            }

            if($seekerCity == $jobCity) {
                $cityScore = .25;
            }

            if($seekerState == $jobState) {
                $stateScore = .25;
            }

            if($seekerCountry == $jobCountry) {
                $countryScore = .25;
            }

            $finalScore = $postalCodeScore + $stateScore + $cityScore + $countryScore;
            return $finalScore;

        } else {

            if(strtolower($seekerRelocation) == 'anywhere') {

                $finalScore = 1.0;
                return $finalScore;

            } else {
                if($seekerCountry == $jobCountry) {
                    $countryScore = .25;
                }
            }

            if(strtolower($seekerRelocation) == 'state') {

                $finalScore = (.75 + $countryScore);
                return $finalScore;

            } else {
                if($seekerState == $jobState) {
                    $stateScore = .25;
                }
            }

            if(strtolower($seekerRelocation) == 'city') {
                $finalScore = (.50 + $countryScore + $stateScore);
                return $finalScore;
            }

            if($jobCity == $seekerCity) {
                $cityScore = .25;
            }

            if($jobPostalCode == $seekerPostalCode) {
                $postalCodeScore = .25;
            }

            $finalScore = $countryScore + $stateScore + $cityScore + $postalCodeScore;
            return $finalScore;
        }
    }

    public function getRevisedLocationScore($location_matching_preference = '')
    {
        $locationScore = 0;

        if (empty($this->seeker->addresses)){
            return 0;
        }

        if (empty($this->job->location)){
            return 0;
        }

        $seekerLocations = $this->seeker->addresses;
        $jobLocation = $this->job->location;

        $seekerRelocation = strtolower($this->seeker->expectation->relocation);

        $seekerCountry = isset($seekerLocations[0]->country) ? strtolower($this->formatVocabulary($seekerLocations[0]->country)) : "";
        $seekerState = isset($seekerLocations[0]->state) ? strtolower($this->formatVocabulary($seekerLocations[0]->state)) : "";
        $seekerCity = isset($seekerLocations[0]->city) ? strtolower($seekerLocations[0]->city) : "";


        $jobCountry = isset($jobLocation->country) ? strtolower($this->formatVocabulary($jobLocation->country)) : "";
        $jobState = isset($jobLocation->state) ? strtolower($this->formatVocabulary($jobLocation->state)) : "";
        $jobCity = isset($jobLocation->city) ? strtolower($jobLocation->city) : "";


        if (strtolower($seekerRelocation) == 'no-relocation') {

            if (($jobCountry == $seekerCountry) && ($jobState == $seekerState) && ($jobCity == $seekerCity)) {
                $locationScore = 1;
            } else {
                $locationScore = 0;
            }

        } else {

            if (empty($seekerRelocation) || ($seekerRelocation == 'anywhere')) {

                $locationScore = 1;

            } else {

                if ($seekerRelocation == 'state') {

                    if ($jobState == $seekerState) {

                        if ($jobCity == $seekerCity) {

                            $locationScore = 1;

                        } else {

                            $locationScore = 0.9;
                        }

                    } else {

                        $locationScore = 0;
                    }

                } else {

                    if ($jobCity == $seekerCity) {

                        $locationScore = 1;

                    } else {

                        $locationScore = 0;
                    }
                }
            }
        }

        $this->locationScore = $locationScore;

        return $locationScore;

    }

    private function reverseLanguageLevel($level)
    {
        $level = (int) $level;
        return $level;
        // @Todo Raju commented this line to fix language matching
        //        return 4 - $level;
    }

    private function formatVocabulary($str)
    {
        $strArr = explode('^^', $str);
        return empty($strArr[0]) ? '' : $strArr[0];
    }

    /**
     * @return bool
     */
    private function jobExperienceMissing()
    {
        if(empty($this->job->soc) && (empty($this->job->duration) || $this->job->duration == 0)){

            if(!empty($this->job->experience_custom_soc)) {
                if(isset($this->job->experience_custom_soc->title)) {
                    return false;
                } else {
                    return true;
                }
            }

            return true;
        }

        return false;
    }
}