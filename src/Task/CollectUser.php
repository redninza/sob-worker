<?php

namespace Task;

class CollectUser extends Base
{
    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload());
        $userInfo = $workload->user;

        $this->showStatus('Retrieving all users...');

        $endpoint = $this->conf['service']['endpoint'] . '/users/admin';
        $http = new \HttpRequest($endpoint);
        $http->setMethod($http::METH_GET);
        $http->send();

        $users = json_decode($http->getResponseBody());

        if (count($users) > 0) {
            foreach($users as $user) {
                $user_id = $user->id;
                $avatar = $user->logo;

                $this->showStatus('Updated avatar link is '. $avatar);

                $endpoint = $this->conf['service']['endpoint'] . '/thread/avatar/update/' . $user_id;
                $http = new \HttpRequest($endpoint);
                $http->setMethod($http::METH_GET);
                $http->send();

                $threads = json_decode($http->getResponseBody());

                if (!empty($threads)) {
                    foreach($threads as $key => $threadId) {
                        $endpoint = $this->conf['service']['endpoint'] . '/thread/avatar/update/' . $threadId . '/' . $user_id;
                        $http = new \HttpRequest($endpoint);
                        $http->setPostFields(array('avatars' => $avatar));
                        $http->setMethod($http::METH_POST);
                        $http->send();

                        $update = json_decode($http->getResponseBody());

                        $this->showStatus($update->message);
                    }

                } else {
                    $this->showStatus('No threads found for user id '. $user_id);
                }
            }
        }
    }
}