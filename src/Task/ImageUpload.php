<?php

namespace Task;

class ImageUpload extends Base
{
    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload());
        $image_path = $workload->image_path;
        $user_id = $workload->user_id;

        // fetch the image and send it to amazon s3 server
        $s3 = new \S3($this->conf['s3']['accesskey'], $this->conf['s3']['secretkey']);
        $file = $s3->putObject(\S3::inputFile($image_path), $this->conf['avatar']['bucket'], $this->conf['avatar']['uri'], \S3::ACL_PUBLIC_READ);

        // update with the amazon link to user collection with user_id

        $endpoint = $this->conf['service']['endpoint'] . '/index_test.php/user/' . $user_id;

        $http = new \HttpRequest($endpoint);
        $http->setMethod($http::METH_PUT);
        $http->send();

        $responseBody = $http->getResponseBody();
        $accountInfo = json_decode($responseBody);
    }
}