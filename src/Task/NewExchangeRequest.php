<?php

namespace Task;

use Service\Notifier\Notifier;
use Service\Notifier\Parse;

class NewExchangeRequest extends Base
{

    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload(), 1);

        $host            = $workload['host'];
        $user            = $workload['user'];
        $otherParentUser = $workload['otherParentUser'];
        $subject         = $workload['subject'];
        $previous        = $workload['previous'];
        $exchangeReq     = $workload['exchangeReq'];
        $date            = $workload['date'];
        $usersKidTypeOne = $workload['usersKidTypeOne'];
        $kidId           = $workload['kidId'];
        $comment         = $workload['comment'];

        $notificationReceiver = $otherParentUser['id'];

        // Email Notification
        $template = $this->twig->loadTemplate('new_exchange_day_request.html.twig');

        $messageBody = $template->render(
            array(
                'otherParentUserName' => $otherParentUser['name'],
                'kidExchangePrev'     => $previous,
                'kidExchange'         => $exchangeReq,
                'name'                => $user['name'],
                'opName'              => $otherParentUser['name'],
                'comment'             => isset($comment[$notificationReceiver]) ? $comment[$notificationReceiver] : '',
                'selectedDate'        => $date,
                'kids'                => $usersKidTypeOne,
                'ownKid'              => $kidId,
                'host'                => $host,
            )
        );

        $message = new \Swift_Message();
        $message->setFrom($this->conf['sender']['email'], $this->conf['sender']['name']);
        $message->setTo($otherParentUser['email']);
        $message->setBody($messageBody, 'text/html');
        $message->setSubject($subject);

        $this->mailerPreferences->setCacheType('array');

        $this->mailer->getTransport()->start();
        $mailStatus = $this->mailer->send($message);
        $this->mailer->getTransport()->stop();


        // Real Time notification
        $notification = json_encode(array('code' => 200, 'message' => $subject, 'active' => 0));

        $pusherConfig = $this->conf['pusher'];
        $notifier     = new Notifier($pusherConfig['key'], $pusherConfig['secret'], $pusherConfig['appid']);
        $response     = $notifier->trigger('private-' . $notificationReceiver, 'NewExchangeRequest', $notification);

        $this->showStatus('trigger response: ');
        var_dump($response);

        if ($response) {
            $this->showStatus('Pusher Event FIRED to private-' . $notificationReceiver . ' channel for NewExchangeRequest');
        } else {
            $this->showStatus('Pusher Event FAILED for private-' . $notificationReceiver . ' channel for NewExchangeRequest');
        }

        // @todo: need to check the users among the receivers if they are online or not --- execute the following code only if they are online

        // push notification to devices
        $pushData = array(
            'code'   => 200,
            'alert'  => $subject,
            'active' => 0,
        );

        $parseConfig  = $this->conf['parse'];
        $parse        = new Parse($parseConfig['appid'], $parseConfig['rest_key'], $parseConfig['master_key']);
        $pushResponse = $parse->trigger($notificationReceiver, $pushData, 'channel');

        $this->showStatus('push response: ');
        var_dump($pushResponse);

        if ($pushResponse) {
            $this->showStatus('push notification SENT to device for userId ' . $notificationReceiver . ' for NewExchangeRequest');
        } else {
            $this->showStatus('push notification FAILED to device for userId ' . $notificationReceiver . ' for NewExchangeRequest');
        }
    }
}