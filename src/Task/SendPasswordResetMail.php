<?php

namespace Task;

class SendPasswordResetMail extends Base
{
    public function run(\GearmanJob $job)
    {
        $user = json_decode($job->workload());
        $user = $user->user;
        $sender = $this->conf['sender'];

        if ($this->validateResetEmailData($user) == false) {
            $this->showStatus('Could not sent email to ' . $user->email . ' for resetting password');
            exit;
        }
        $template = $this->twig->loadTemplate('send_password_reset_template_plain.php.twig');

        $link = 'http://' . $user->extra_params->host . '/auth/resetPassword/'. $user->password_hash. '/'. $user->id;

        $messageBody =  $template->render(
                            array(
                                'email' => $user->email,
                                'firstName' => $user->first_name,
                                'lastName' => $user->last_name,
                                'reset_link' => $link
                            )
                        );

       /* $message = new \Swift_Message();
        $message->setFrom($sender['email'], $sender['name']);
        $message->setTo($user->email);
        $message->setSubject('Forget Password');
        $message->setBody($messageBody, 'text/html');

        $mailStatus = $this->mailer->send($message);
        $this->showStatus('Password reset email sent to: ' . $user->email);
        $this->showStatus('Total number of email sent: ' . $mailStatus);*/

        $sendGrid = new \Service\Mailer\Mailer($this->conf);
        $sendGrid->mailer->setFrom($sender['email']);
        $sendGrid->mailer->setFromName($sender['name']);
        $sendGrid->mailer->setTo($user->email);
        $sendGrid->mailer->setSubject('Forgot Your Password?');
        $sendGrid->mailer->setHtml($messageBody);

        $mailStatus = $sendGrid->send();

        $this->showStatus('Password reset email sent to: ' . $user->email);
        $this->showStatus('Email sending status: ' . $mailStatus->message );
    }

    protected function validateResetEmailData($user)
    {
        if(empty($user->extra_params->host)) return false;
        if(empty($user->password_hash)) return false;
        if(empty($user->id)) return false;

        return true;
    }
}