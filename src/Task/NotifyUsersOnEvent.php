<?php

namespace Task;

use Service\Notifier\Notifier;
use Service\Notifier\Parse;

class NotifyUsersOnEvent extends Base
{

    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload(), 1);

        $receivers = $workload['receivers'];
        $name      = $workload['name'];
        $date      = $workload['date'];
        $action    = $workload['action'];
        $sender    = $workload['sender'];

        $subject = "";
        $acted = "";

        if ($action == 'insert') {
            $subject = "You have been added to an event named '{$name}' on {$date} by {$sender}";
            $acted = "EventAdded";

        } elseif ($action == 'update') {

            $subject = "One of your event named '{$name}' on {$date} is updated by {$sender}";
            $acted = "EventUpdated";
        }

        // Real Time notification
        $notification = json_encode(array('code' => 200, 'message' => $subject, 'active' => 0));

        $pusherConfig = $this->conf['pusher'];
        $notifier     = new Notifier($pusherConfig['key'], $pusherConfig['secret'], $pusherConfig['appid']);

        foreach ($receivers as $notificationReceiver) {
            $response = $notifier->trigger('private-' . $notificationReceiver, $acted, $notification);

            $this->showStatus('trigger response: ');
            var_dump($response);

            if ($response) {
                $this->showStatus('Pusher Event FIRED to private-' . $notificationReceiver . ' channel for ' . $acted);
            } else {
                $this->showStatus('Pusher Event FAILED for private-' . $notificationReceiver . ' channel for ' . $acted);
            }
        }
        // @todo: need to check the users among the receivers if they are online or not --- execute the following code only if they are online



        // push notification to devices
        $pushData = array(
            'code'   => 200,
            'alert'  => $subject,
            'active' => 0,
        );

        $parseConfig  = $this->conf['parse'];
        $parse        = new Parse($parseConfig['appid'], $parseConfig['rest_key'], $parseConfig['master_key']);


        foreach ($receivers as $notificationReceiver) {
            $pushResponse = $parse->trigger($notificationReceiver, $pushData, 'channel');

            $this->showStatus('push response: ');
            var_dump($pushResponse);

            if ($pushResponse) {
                $this->showStatus('push notification SENT to device for userId ' . $notificationReceiver . ' for ' . $acted);
            } else {
                $this->showStatus('push notification FAILED to device for userId ' . $notificationReceiver . ' for ' . $acted);
            }
        }
    }
}