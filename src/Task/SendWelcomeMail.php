<?php

namespace Task;

class SendWelcomeMail extends Base
{
    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload());
        $user = $workload->user;

        $link = 'http://' . $this->conf['web']['endpoint'].'/login';
        $sender = $this->conf['sender'];

        $firstName  = empty($user->first_name) ? $user->email : $user->first_name;
        $lastName  = empty($user->last_name) ? '' : $user->last_name;

        $mode = $user->settings->provider_mode;

        if ($mode == 1) {
            $providerMode = 1;
        } else {
            $providerMode = 0;
        }

        $this->showStatus('Loading mail template.');

        if ($providerMode) {
            $messageTemplate = $this->twig->loadTemplate('provider_welcome_mail.html.twig');
        } else {
            $messageTemplate = $this->twig->loadTemplate('seeker_welcome_mail.html.twig');
        }

        $messageBody = $messageTemplate->render(
            array(
                'email' => $user->email,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'link' => $link
            )
        );

        $message = new \Swift_Message();
        $message->setFrom($sender['email'], $sender['name']);
        $message->setTo($user->email);
        $message->setSubject('Welcome to Loosemonkies');
        $message->setBody($messageBody, 'text/html');

        $this->mailerPreferences->setCacheType('null');

        $this->mailer->getTransport()->start();
        $mailStatus = $this->mailer->send($message);
        $this->mailer->getTransport()->stop();

        $this->showStatus('Account activation welcome email sent to: ' . $user->email);
    }
}