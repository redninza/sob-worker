<?php

namespace Task;

use Service\Notifier\Notifier;
use Service\Notifier\Parse;

class UpdateExchangeRequest extends Base
{

    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload(), 1);

        $host      = $workload['host'];
        $receiver  = $workload['receiver'];
        $subject   = $workload['subject'];
        $actedOn   = $workload['actedOn'];

        $notificationReceiver = $receiver['id'];

        // Real Time notification
        $notification = json_encode(array('code' => 200, 'message' => $subject, 'active' => 0));

        $pusherConfig = $this->conf['pusher'];
        $notifier     = new Notifier($pusherConfig['key'], $pusherConfig['secret'], $pusherConfig['appid']);
        $response     = $notifier->trigger('private-' . $notificationReceiver, 'UpdateExchangeRequest', $notification);

        $this->showStatus('trigger response: ');
        var_dump($response);

        if ($response) {
            $this->showStatus('Pusher Event FIRED to private-' . $notificationReceiver . ' channel for '. $actedOn . 'UpdateExchangeRequest');
        } else {
            $this->showStatus('Pusher Event FAILED for private-' . $notificationReceiver . ' channel for '. $actedOn . 'UpdateExchangeRequest');
        }

        // @todo: need to check the users among the receivers if they are online or not --- execute the following code only if they are online

        // push notification to devices
        $pushData = array(
            'code'   => 200,
            'alert'  => $subject,
            'active' => 0,
        );

        $parseConfig  = $this->conf['parse'];
        $parse        = new Parse($parseConfig['appid'], $parseConfig['rest_key'], $parseConfig['master_key']);
        $pushResponse = $parse->trigger($notificationReceiver, $pushData, 'channel');

        $this->showStatus('push response: ');
        var_dump($pushResponse);

        if ($pushResponse) {
            $this->showStatus('push notification SENT to device for userId ' . $notificationReceiver . ' for '. $actedOn . 'UpdateExchangeRequest');
        } else {
            $this->showStatus('push notification FAILED to device for userId ' . $notificationReceiver . ' for '. $actedOn . 'UpdateExchangeRequest');
        }
    }
}