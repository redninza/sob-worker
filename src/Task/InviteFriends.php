<?php

namespace Task;

class InviteFriends extends Base
{
    public function run(\GearmanJob $job)
    {
        $sender = $this->conf['sender'];

        $workload = json_decode($job->workload(), 1);
        $data = $workload['data'];

        $host       = $data['host'];
        $userId     = $data['userId'];
        $senderName = $data['senderName'];
        $emails     = $data['emails'];
        /*$subject    = $data['subject'];*/
        $subject    = $data['senderName'] . " has invited you to join Loosemonkies.com";
        $emailBody  = $data['body'];

        $i = 0;
        foreach ($emails as $email) {
            $temp = explode('@', $email);
            $name = $temp[0];

            $template = $this->twig->loadTemplate('send_invite_friends_mail.php.twig');

            $link = 'http://' . $host . '/?ref='. $userId;

            $messageBody =  $template->render(
                array(
                    'email' => $email,
                    'name'  => $name,
                    'senderName'  => $senderName,
                    'body'  => $emailBody,
                    'link'  => $link
                )
            );

            /*$message = new \Swift_Message();
            $message->setFrom($sender['email'], $sender['name']);
            $message->setTo($email);
            $message->setSubject($subject);
            $message->setBody($messageBody, 'text/html');

            $this->mailerPreferences->setCacheType('null');

            $this->mailer->getTransport()->start();
            $mailStatus = $this->mailer->send($message);
            $this->mailer->getTransport()->stop();*/

            $sendGrid = new \Service\Mailer\Mailer($this->conf);
            $sendGrid->mailer->setFrom($sender['email']);
            $sendGrid->mailer->setFromName($sender['name']);
            $sendGrid->mailer->setTo($email);
            $sendGrid->mailer->setSubject($subject);
            $sendGrid->mailer->setHtml($messageBody);

            $mailStatus = $sendGrid->send();

            $this->showStatus('Email sending status: ' . $mailStatus->message );
            $this->showStatus('Invitation email sent to: ' . $email);

            $i++;
        }

        $this->showStatus('Total number of email sent: ' . $i);
    }
}