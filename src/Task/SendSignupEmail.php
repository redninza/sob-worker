<?php

namespace Task;

class SendSignupEmail extends Base
{
    public function run(\GearmanJob $job)
    {
        $user = json_decode($job->workload());
        $sender = $this->conf['sender'];

        $template = $this->twig->loadTemplate('new_account.html.twig');

        $link = 'http://' . $user->extra_params->host . '/login';
        $activationLink = 'http://' . $user->extra_params->host . '/auth/activateAccount/' . $user->confirmation_token;

        $messageBody = $template->render(array(
            'email'     => $user->email,
            'firstName' => $user->firstName,
            'lastName'  => $user->lastName,
            'link'      => $link,
            'activation_link' => $activationLink
        ));

       /* $message = new \Swift_Message();
        $message->setFrom(array($sender['name'] => $sender['email']));
        $message->setTo(array($user->email));
        $message->setBody($messageBody, 'text/html');

        $this->mailer->send($message);*/

        $sendGrid = new \Service\Mailer\Mailer($this->conf);
        $sendGrid->mailer->setFrom($sender['email']);
        $sendGrid->mailer->setFromName($sender['name']);
        $sendGrid->mailer->setTo($user->email);
        $sendGrid->mailer->setSubject('Loosemonkies Secrets');
        $sendGrid->mailer->setHtml($messageBody);

        $mailStatus = $sendGrid->send();
    }
}