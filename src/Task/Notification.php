<?php

namespace Task;

class Notification extends Base
{
    public function run(\GearmanJob $job)
    {
        $workload = json_decode($job->workload());
        $notification = $workload->notification;
        $webhost      = $workload->webhost;

        $notificationReceiver = $notification->receiver;

        if(!is_object($notification->create_date)) {
            $notification->create_date = new \DateTime($notification->create_date);
        }

        // firing event to pusher
        $pusherConfig = $this->conf['pusher'];
        $notifier = new \Service\Notifier\Notifier($pusherConfig['key'], $pusherConfig['secret'], $pusherConfig['appid']);
        $response = $notifier->trigger('private-'.$notificationReceiver, 'notification', $notification);

        $this->showStatus('trigger response: ');
        print_r($response);

        if($response){
            $this->showStatus('Pusher Event Fired to private-' . $notificationReceiver . ' channel for ' . $notification->action);
        } else {
            $this->showStatus('Pusher Event FAILED for private-' . $notificationReceiver . ' channel for ' . $notification->action);
        }


        // retrieving user information from receiver for mailing
        $http = new \HttpRequest($this->conf['service']['endpoint'] . '/users/'.$notificationReceiver);
        $http->setMethod($http::METH_GET);
        $http->send();

        $responseCode = $http->getResponseCode();
        $responseBody = $http->getResponseBody();

        if ($responseCode == 200) {
            $user = json_decode($responseBody);

            $this->showStatus('Retrived user info.');

            // mailing the event notification to receiver
            if ($notification->action == "seeker-favorite"){
                $template = $this->twig->loadTemplate('noitification_for_favoriting_a_seeker.html.twig');

            } else {

                $template = $this->twig->loadTemplate('activity_notification_mail.html.twig');
            }

            $sender = $this->conf['sender'];

            $notificationType = $this->getNotificationType($notification->action, $notification->data);

            if (($notification->action != 'chat-enable') && ($notification->action != 'public-job-view')) {
                $this->showStatus('Sender is : ' . $sender['email']);

                $messageSubject = "You have a new Notification!";

                $userSettings = $user->settings;
                $poroviderMode = $userSettings->provider_mode;

                $messageBody = $template->render(array(
                        'email'             => $user->email,
                        'firstname'         => $user->first_name,
                        'middlename'        => $user->middle_name,
                        'lastname'          => $user->last_name,
                        'providermode'      => ($poroviderMode) ? 1 : 0,
                        'notificationType'  => $notificationType,
                        'longinLink'        => $webhost . '/login'
                    )
                );

                /*$message = new \Swift_Message();
                $message->setFrom($sender['email'], 'Loosemonkies');
                $message->setTo($user->email);
                $message->setBody($messageBody, 'text/html');
                $message->setSubject($messageSubject);

                $this->mailerPreferences->setCacheType('null');

                $this->mailer->getTransport()->start();
                $mailStatus = $this->mailer->send($message);
                $this->mailer->getTransport()->stop();*/

                $sendGrid = new \Service\Mailer\Mailer($this->conf);
                $sendGrid->mailer->setFrom($sender['email']);
                $sendGrid->mailer->setFromName($sender['name']);
                $sendGrid->mailer->setTo($user->email);
                $sendGrid->mailer->setSubject($messageSubject);
                $sendGrid->mailer->setHtml($messageBody);

                $mailStatus = $sendGrid->send();

                $this->showStatus('Notification email sent to: ' . $user->email);
                $this->showStatus('Email sending status: ' . $mailStatus->message );

                if ($mailStatus->message == 'success') {
                    $this->showStatus('Notification email sent to receiver: "' . $user->email . '"');
                } else {
                    $this->showStatus('Notification email NOT SENT to receiver: "' . $user->email . '"');
                }
            }
        }
    }

    protected function getNotificationType($action, $data)
    {
        $type = "";

        switch($action)
        {
            case "job-apply"             :  $type = html_entity_decode($data->seeker_name) . ' has applied to your job: ' . html_entity_decode($data->job_title);
                                            break;

            case "job-favorite"          :  $type = html_entity_decode($data->seeker_name) . ' favorited your job: ' . html_entity_decode($data->job_title);
                                            break;

            case "seeker-favorite"       :  $type = html_entity_decode($data->company_name) . ' marked you as favorite seeker for the job: ' . html_entity_decode($data->job_title);
                                            break;

            case "seeker-profile-view"   :  $type = html_entity_decode($data->company_name) . ' viewed your profile for: ' . html_entity_decode($data->job_title);
                                            break;

            case "chat-enable"           :  $type = 'A seeker who applied to one of your job has enabled chat';
                                            break;

            case "chat-initiate"         :  $type = 'A new chat has been initiated on your job application for : ' . html_entity_decode($data->job_title);
                                            break;

            case "seeker-disqualify"     :  $type = 'Some one disqualified from your applied job: ' . html_entity_decode($data->job_title);
                                            break;

            case "public-job-view"       :  $type = html_entity_decode($data->job_title) . ' viewed by you';
                                            break;
        }

        return $type;
    }

}