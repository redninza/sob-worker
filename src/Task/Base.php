<?php

namespace Task;

use Service\HttpClient\Factory;

abstract class Base
{
    /** @var array */
    protected $conf;

    /** @var \Twig_Environment */
    protected $twig;

    /** @var \Swift_Mailer */
    protected $mailer;

    /** @var string */
    protected $function;

    /** @var \GearmanClient */
    protected $gearman;

    /** @var \Service\HttpClient\HttpClientInterface */
    protected $httpClient;

    /** @var \Swift_Plugins_Logger */
    protected $mailerLogger;

    /** @var \Swift_Preferences */
    protected $mailerPreferences;

    public function __construct($conf, $twig, $mailer, $mailerLogger, $mailerPreferences)
    {
        $this->conf    = $conf;
        $this->twig    = $twig;
        $this->mailer  = $mailer;
        $this->mailerPreferences  = $mailerPreferences;
        $this->mailerLogger = $mailerLogger;

        $this->setupGearman();
        //$this->setupHttpClient();
        $this->setFunction();
    }

    private function setupGearman()
    {
        $this->gearman = new \GearmanClient();
        $this->gearman->addServer($this->conf['gearman']['host'], $this->conf['gearman']['port']);
    }

    protected function setupHttpClient()
    {
        $this->httpClient = Factory::get('HttpRequest');
    }

    protected function addBackgroundTask($event, $data)
    {
        return $this->gearman->addTaskBackground($event, json_encode($data));
    }

    protected function runTasks()
    {
        $this->gearman->runTasks();
    }

    public function getFunction()
    {
        return $this->function;
    }

    public function showStatus($msg)
    {
        echo '[' . date('Y-m-d H:i:s') . '] ' . $this->getFunction() . ': ' . $msg . PHP_EOL;
    }

    protected function setFunction()
    {
        $this->function = str_replace('Task\\', '', get_class($this));
    }

    abstract public function run(\GearmanJob $job);
}