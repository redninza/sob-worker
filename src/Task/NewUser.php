<?php

namespace Task;

class NewUser extends Base
{
    public function run(\GearmanJob $job)
    {
        $this->showStatus('Sending Signup Email.');

        $workload = json_decode($job->workload());
        $user     = $workload->user;

        $sender       = $this->conf['sender'];
        $userSettings = $user->settings;

        $this->showStatus('user settings: ' . json_encode($userSettings));

        if ($this->validateSignupEmailData($user) == false) {
            $this->showStatus('Signup email validation failed.');
            exit;
        }

        $link = 'http://' . $user->extra_params->host . '/login';
        $activationLink = 'http://' . $user->extra_params->host . '/auth/activateAccount/' . $user->confirmation_token;

        $firstName = empty($user->first_name) ? $user->email : $user->first_name;
        $lastName  = empty($user->last_name) ? '' : $user->last_name;

        $this->showStatus('Prepare Signup email data.');

        if ($userSettings->provider_mode) {
            $this->showStatus('Signup email provider mode true.');
            $messageTemplate = $this->twig->loadTemplate('new_provider_account_plain.html.twig');
        } else {
            $this->showStatus('Signup email seeker mode true.');
            $messageTemplate = $this->twig->loadTemplate('new_seeker_account_plain.html.twig');
        }

        $this->showStatus('Signup seeker and provider mode track.');

        $messageBody = $messageTemplate->render(
            array(
                'email'           => $user->email,
                'firstName'       => $firstName,
                'lastName'        => $lastName,
                'link'            => $link,
                'activation_link' => $activationLink
            )
        );

        $sendGrid = new \Service\Mailer\Mailer($this->conf);
        $sendGrid->mailer->setFrom($sender['email']);
        $sendGrid->mailer->setFromName($sender['name']);
        $sendGrid->mailer->setTo($user->email);
        $sendGrid->mailer->setSubject('Loosemonkies Secrets');
        $sendGrid->mailer->setHtml($messageBody);

        $mailStatus = $sendGrid->send();

        $this->showStatus('Signup email sent to: ' . $user->email);
        $this->showStatus('Email sending status: ' . $mailStatus->message );

        if ($mailStatus->message == 'success') {
            $this->showStatus('Updating activation mail sending date for id: ' . $user->id);

            $this->showStatus('path: ' . $this->conf['service']['endpoint'] . '/users/worker/updateActivationDate/' . $user->id);

            $http = new \HttpRequest($this->conf['service']['endpoint'] . '/users/worker/updateActivationDate/' . $user->id);
            $http->addHeaders(array('Content-type' => 'application/x-www-form-urlencoded'));
            $http->setMethod($http::METH_PUT);
            $http->send();

            $response = $http->getResponseBody();
            $updatedUser = json_decode($response);

            $code = $http->getResponseCode();

            if ($code == 200) {
                $this->showStatus('activation date updated  ');
            } else {
                $this->showStatus('activation date IS NOT updated ');
            }
        }
    }

    protected function validateSignupEmailData($user)
    {
        $valid = empty($user->extra_params->host) ? false : true;
        //$valid = empty($user->confirmation_token) ? false : true;
        $valid = empty($user->id) ? false : true;
        $valid = empty($user->email) ? false : true;

        return $valid;
    }
}