<?php

require_once __DIR__ . '/../bootstrap.php';
use Model\Matching;

class MatchingTest extends \PHPUnit_Framework_TestCase
{
    public function testScoringWorksForAllSections()
    {
        $job = $this->getJob();
        $seeker = $this->getSeeker();

        global $config;
        $matching = new Matching($job, $seeker, $config);
        $scores = $matching->getScores(1);

        $this->assertArrayHasKey('skill_score', $scores);
        $this->assertArrayHasKey('experience_score', $scores);
        $this->assertArrayHasKey('education_score', $scores);
        $this->assertArrayHasKey('language_score', $scores);
        $this->assertArrayHasKey('expectation_score', $scores);
        $this->assertArrayHasKey('final_score', $scores);
    }

    private function getSeeker()
    {
        $json = '{
                    "id":"50a9d76e35e8a6a658000012",
                    "total_employment_duration":null,
                    "summary":"I am a 6 years experienced software professional having experience in Managing software projects and managing IT process and many of it\'s components like Software development, Software Quality assurance. I have working experience in enterprise level software and IT solution (developing, maintain and managing them). Through my working experience and thought process - I am able to take any challenge in the field of managing and coordinating any software and IT process.\n\nGenerally, in an environment where people can work with self-motivation and scope for applying innovation, my best outputs are available. I want to work with responsibility in an environment where I can enrich my career in a process of contributing to the organization goal attainment.",
                    "tagline":"Project Manager at LooseMonkies",
                    "data_source":null,
                    "contacts":{
                                "skype":"red.ninza",
                                "mobile":"+8801711664049",
                                "twitter":"nagbaba"
                                },
                    "social_medias":null,
                    "notification_preference":null,
                    "privacy_preference":null,
                    "applied_count":null,
                    "favorite_count":null,
                    "opportunity_count":null,
                    "expiry_notification":null,
                    "goals":"Want to build a career in Product development",
                    "interests":"MIS, Web, Graphics, sleeping",
                    "current_engagement":null,
                    "current_engagement_type":null,
                    "user":{
                            "id":"50a9d76c35e8a60d59000002",
                            "email":"himel+lm+linkedin@loosemonkies.com",
                            "first_name":"Himel",
                            "middle_name":"Nag",
                            "last_name":"Rana",
                            "avatar":"http:\/\/m3.licdn.com\/mpr\/mprx\/0_wM8zEmvsuDy680JkwOGQEDNzaHUk8JekWjNEE2cHAm7NxgxXFxKJXu_6hLRUiOWeEsCI5wL-U9wx",
                            "enabled":true,
                            "last_login":{
                                            "date":"2012-11-19 09:04:33",
                                            "timezone_type":3,
                                            "timezone":"UTC"
                                         },
                            "settings":{
                                        "inactitvity_auto_timeout":"0",
                                        "disable_session_timeout":"0",
                                        "language":"en",
                                        "disable_sounds":false,
                                        "calendar_weeks":"4",
                                        "facebook_feed":"",
                                        "twitter_feed":"",
                                        "send_job_match_update":"0",
                                        "send_monthly_newsletter":"0",
                                        "provider_mode":"0",
                                        "auto_enable_live_chat":"0",
                                        "default_search_radius_km":25,
                                        "appear_offline":false,
                                        "allow_provider_to_see_profile":true,
                                        "allow_provider_to_contact":true
                                       },
                            "create_date":{
                                            "date":"2012-11-19 06:53:32",
                                            "timezone_type":3,
                                            "timezone":"UTC"
                                          },
                            "update_date":{
                                            "date":"2012-11-19 09:05:26",
                                            "timezone_type":3,
                                            "timezone":"UTC"
                            }
                    },
                    "educations":[
                                    {
                                        "id":"6e53a7cc5ed40f06918c380298462613",
                                        "institute":"University of Dhaka",
                                        "passing_year":"2009",
                                        "year_to":null,
                                        "year_from":null,
                                        "grade_type":null,
                                        "result":"null",
                                        "degree_name":"MBA",
                                        "degree":"14^^Some Post-Graduate",
                                        "department":"MIS",
                                        "faculty":"MIS",
                                        "city":"",
                                        "state":null,
                                        "country":null,
                                        "specialization":null,
                                        "field_of_study": "100331: Engineering",
                                        "notes":null
                                    },
                                    {
                                        "id":"fed7ce656d4ad48ab646ec2c6a4f1b6b",
                                        "institute":"Shahjalal University of Science and Technology",
                                        "passing_year":"2006",
                                        "year_to":null,
                                        "year_from":null,
                                        "grade_type":null,
                                        "result":"null",
                                        "degree_name":"B.Sc. Engineering",
                                        "degree":"12^^Bachelors Degree",
                                        "department":"CSE",
                                        "faculty":"CSE",
                                        "city":"",
                                        "state":null,
                                        "country":null,
                                        "specialization":null,
                                        "field_of_study": "100331: Engineering",
                                        "notes":null
                                    },
                                    {
                                        "id":"863a4dc94f0bb894f3d222238393fc37",
                                        "institute":"Notre Dame College",
                                        "passing_year":"2000",
                                        "year_to":null,
                                        "year_from":null,
                                        "grade_type":null,
                                        "result":"null",
                                        "degree_name":"HSC",
                                        "degree":"6^^Some College",
                                        "department":"Science",
                                        "faculty":"Science",
                                        "city":"",
                                        "state":null,
                                        "country":null,
                                        "specialization":null,
                                        "field_of_study": "100331: Engineering",
                                        "notes":null
                                    }
                                ],
                    "experiences":[
                                    {
                                        "id":"dae43fd2d6c97737e8c0663d7f35b9f7",
                                        "company_name":"Loosemonkies",
                                        "start_date":null,
                                        "end_date":null,
                                        "start_year":2011,
                                        "end_year":null,
                                        "designation":"PROJECT MANAGER",
                                        "details":"Here my responsibilities are ----- -- Managing the Development, QA and UI & UX team. -- Managing the Loosemonkies Project -- Working for all R&D and regular development and Reporting.",
                                        "is_current":true,
                                        "duration":null,
                                        "soc":"11-3061",
                                        "company_city":"",
                                        "company_state":null,
                                        "company_country":null,
                                        "company_url":null,
                                        "job_category":null,
                                        "notes":null,
                                        "industry":{
                                                    "id":"507eb4c235e8a68417000025",
                                                    "name":"null",
                                                    "slug":"null-507eb4c235e8a68417000025",
                                                    "sort_order":0
                                                   },
                                        "company":{
                                                    "id":"50a9d76e35e8a6a65800000f",
                                                    "name":null,
                                                    "slug":null,
                                                    "about":null,
                                                    "logo":null,
                                                    "url":null,
                                                    "verified":null,
                                                    "contacts":null,
                                                    "socialMedias":null,
                                                    "company_settings":null,
                                                    "corporate_info":null
                                                   }
                                    },
                                    {
                                        "id":"b4e87af903cfea5c28aea57e91b806fa",
                                        "company_name":"BJIT Limited",
                                        "start_date":null,
                                        "end_date":null,
                                        "start_year":2010,
                                        "end_year":2011,
                                        "designation":"Software Developer\/Support Engineer",
                                        "details":"Here I am working with an off-shore development team and working on a IT business solution software",
                                        "is_current":false,
                                        "duration":1,
                                        "soc": "",
                                        "experience_custom_soc": {
                                             "title": "Software Architect",
                                             "soc1": "15",
                                             "soc2": "10",
                                             "soc3": "31"
                                        },
                                        "company_city":null,
                                        "company_state":null,
                                        "company_country":null,
                                        "company_url":null,
                                        "job_category":null,
                                        "notes":null,
                                        "industry":{
                                                    "id":"507eb4c235e8a68417000025",
                                                    "name":"null",
                                                    "slug":"null-507eb4c235e8a68417000025",
                                                    "sort_order":0
                                                   },
                                        "company":{
                                                    "id":"50a9d76e35e8a6a658000010",
                                                    "name":null,
                                                    "slug":null,
                                                    "about":null,
                                                    "logo":null,
                                                    "url":null,
                                                    "verified":null,
                                                    "contacts":null,
                                                    "socialMedias":null,
                                                    "company_settings":null,
                                                    "corporate_info":null
                                                  }
                                    },
                                    {
                                        "id":"f21ea9e5be9e90f5d186b38d133c993a",
                                        "company_name":"ECBB Bangladesh Limited",
                                        "start_date":null,
                                        "end_date":null,
                                        "start_year":2007,
                                        "end_year":2010,
                                        "designation":"Web Application Developer",
                                        "details":"Developing complex web applications like CMS, SNS. My tasks there was as requirement analyst and developer.",
                                        "is_current":false,
                                        "duration":3,
                                        "soc":"15-1081",
                                        "company_city":null,
                                        "company_state":null,
                                        "company_country":null,
                                        "company_url":null,
                                        "job_category":null,
                                        "notes":null,
                                        "industry":{
                                                    "id":"507eb4c235e8a68417000025",
                                                    "name":"null",
                                                    "slug":"null-507eb4c235e8a68417000025",
                                                    "sort_order":0
                                                   },
                                        "company":{
                                                    "id":"50a9d76e35e8a6a658000011",
                                                    "name":null,
                                                    "slug":null,
                                                    "about":null,
                                                    "logo":null,
                                                    "url":null,
                                                    "verified":null,
                                                    "contacts":null,
                                                    "socialMedias":null,
                                                    "company_settings":null,
                                                    "corporate_info":null
                                                  }
                                    }
                                  ],
                    "addresses":[
                                    {
                                        "id":"50a9d76e35e8a6a658000014",
                                        "street":null,
                                        "city":"Brent",
                                        "state":"FL^^Florida",
                                        "postal_code":"123121",
                                        "country":"US^^United States",
                                        "lat":null,
                                        "lon":null,
                                        "is_current":null
                                    }
                                ],
                    "language":[
                                    {
                                        "id":"ada8211eceba21e555a241a0dc9ed50b",
                                        "name":"English",
                                        "reading_level":"2^^Moderate",
                                        "speaking_level":"2^^Moderate",
                                        "writing_level":"2^^Moderate"
                                    }
                                ],
                    "skills":[
                                {
                                    "id":"c9560a08e4a4ad9a2cbeccfe6113fa82",
                                    "keywords":"MySQL",
                                    "level":"3^^Expert",
                                    "years":4,
                                    "category":"General"
                                },
                                {
                                    "id":"c2921a5a7b32487b905b80b4008c0c3e",
                                    "keywords":"Zend Framework",
                                    "level":"3^^Expert",
                                    "years":4,
                                    "category":"General"
                                },
                                {
                                    "id":"bab55a89d11c393e37241c7f506526c2",
                                    "keywords":"jQuery",
                                    "level":"2^^Intermediate",
                                    "years":1,
                                    "category":"General"
                                }
                            ],
                    "expectation":{
                                    "id":"50a9d76e35e8a6a658000013",
                                    "engagement":"1^^Contractual",
                                    "term":"1^^Full Time",
                                    "type":"1^^Employee",
                                    "relocation":"country",
                                    "payment_type":"monthly",
                                    "currency":"USD",
                                    "negotiable":null,
                                    "salary_start":15000,
                                    "salary_end":25000
                                  }
                }';


        return json_decode($json);
    }

    private function getJob()
    {
        $json = '{
                    "id":"50a9e45035e8a6a55800000f",
                    "soc":"15-1031",
                    "additional_keywords":"",
                    "title":"Software Developer\/Support Engineer",
                    "slug":"software-developer-support-engineer-50a9e45035e8a6a55800000f",
                    "excerpt":"",
                    "duration":"4",
                    "subtitle":"Responsible for providing technology solutions, maintaining custom and packaged software solutions",
                    "description":"Reviews and analyzes existing applications effectiveness and efficiency, and then develops strategies for improving or leveraging these systems\r\n\r\nCreates, updates, tests, debugs & provides support to enterprise operational and\/or information systems including hands on development with 3rd party software to support claims\/underwriting\/finance\/actuarial department (s)\r\n\r\nPerforms unit test and troubleshoot QA and production issues with business rules\r\n\r\nAssists in providing support to project teams in defining and reviewing product and software requirements",
                    "posting_date":"2012-11-19",
                    "deadline_date":"2012-11-20",
                    "decision_date":"2012-11-21",
                    "job_category":"Project Management",
                    "status":"published",
                    "experience_title":"Software Developer\/Support Engineer",
                    "job_soc":"15-1031",
                    "location":{
                                "id":"50aa091435e8a6a658000044",
                                "street":null,
                                "city":"Brent",
                                "state":"FL^^Florida",
                                "postal_code":"123231",
                                "country":"US^^United States",
                                "lat":null,
                                "lon":null,
                                "is_current":"0"
                               },
                    "languages":[
                                    {
                                        "id":"641d7769d4aaac64648d03a3f9b892b8",
                                        "name":"English",
                                        "reading_level":"1^^Fluent",
                                        "speaking_level":"1^^Fluent",
                                        "writing_level":"1^^Fluent"
                                    }
                                ],
                    "skills":[
                                {
                                    "id":"f8f5b04909ae3f922ea1a0b48294223d",
                                    "keywords":"MySQL",
                                    "level":"4^^Guru",
                                    "years":"4",
                                    "category":"Programming"
                                }
                            ],
                    "company":{
                                "id":"50a9d92535e8a6a258000011",
                                "name":"Inversionz Garage",
                                "slug":"inversionz-garage-50a9d92535e8a6a258000011",
                                "about":"This is a startup but we have a dream to make this the biggest in the industry.",
                                "logo":"http:\/\/ec2-23-22-225-198.compute-1.amazonaws.com\/uploads\/2219918899.png",
                                "url":null,
                                "verified":null,
                                "contacts":null,
                                "socialMedias":null,
                                "addresses":[
                                                {
                                                    "id":"50a9de2d35e8a61066000000",
                                                    "street":"121 Street, 5th Avenue, Downtown",
                                                    "city":"Brent",
                                                    "state":"FL^^Florida",
                                                    "postal_code":"121321",
                                                    "country":"US^^United States",
                                                    "lat":null,
                                                    "lon":null,
                                                    "is_current":null
                                                }
                                            ],
                                "company_settings":{
                                                        "over_qualification_factor_for_skill":null,
                                                        "over_qualification_factor_for_language":null,
                                                        "over_qualification_factor_for_experience":null,
                                                        "over_qualification_factor_for_education":null,
                                                        "age_factor_acceptable":null,
                                                        "age_factor_unacceptable":null,
                                                        "set_base_value_for_advance_fine_tuning_slider":null,
                                                        "base_value_for_advance_fine_tuning_slider":null,
                                                        "display_score_values_over_hundred_percent":null
                                                    },
                                "corporate_info":{
                                                    "established":2010,
                                                    "business_type":0,
                                                    "company_size":10,
                                                    "number_of_locations":1,
                                                    "annual_revenue_start":300000,
                                                    "annual_revenue_end":450000,
                                                    "primary_industry":{
                                                                        "id":"507f8d5b35e8a69e1b00000f",
                                                                        "name":"Alternative Medicine",
                                                                        "slug":"alternative-medicine-507f8d5b35e8a69e1b00000f",
                                                                        "sort_order":0
                                                                       },
                                                    "languages":[
                                                                    {
                                                                        "id":"8d32148185b22043bda0641fac6a3789",
                                                                        "name":"English",
                                                                        "reading_level":null,
                                                                        "speaking_level":null,
                                                                        "writing_level":null
                                                                    }
                                                                ]
                                                }
                                },
                                "posted_by":{
                                                "id":"50a9d76c35e8a60d59000002",
                                                "email":"himel+lm+linkedin@loosemonkies.com",
                                                "first_name":"Himel",
                                                "middle_name":"Nag",
                                                "last_name":"Rana",
                                                "avatar":"http:\/\/m3.licdn.com\/mpr\/mprx\/0_wM8zEmvsuDy680JkwOGQEDNzaHUk8JekWjNEE2cHAm7NxgxXFxKJXu_6hLRUiOWeEsCI5wL-U9wx",
                                                "enabled":true,
                                                "last_login":{
                                                                "date":"2012-11-19 09:04:33",
                                                                "timezone_type":3,
                                                                "timezone":"UTC"
                                                            },
                                                "settings": {
                                                                "inactitvity_auto_timeout":"0",
                                                                "disable_session_timeout":"0",
                                                                "language":"en",
                                                                "disable_sounds":false,
                                                                "calendar_weeks":"4",
                                                                "facebook_feed":"",
                                                                "twitter_feed":"",
                                                                "send_job_match_update":"0",
                                                                "send_monthly_newsletter":"0",
                                                                "provider_mode":"1",
                                                                "auto_enable_live_chat":"0",
                                                                "default_search_radius_km":25,
                                                                "appear_offline":false,
                                                                "allow_provider_to_see_profile":true,
                                                                "allow_provider_to_contact":true
                                                            },
                                                "create_date":{
                                                                "date":"2012-11-19 06:53:32",
                                                                "timezone_type":3,
                                                                "timezone":"UTC"
                                                              },
                                                "update_date":{
                                                                "date":"2012-11-19 10:24:22",
                                                                "timezone_type":3,
                                                                "timezone":"UTC"
                                                               }
                                },
                                "industry":{
                                            "id":"507f8d5b35e8a69e1b00000f",
                                            "name":"Alternative Medicine",
                                            "slug":"alternative-medicine-507f8d5b35e8a69e1b00000f",
                                            "sort_order":0
                                           },
                                "expectation":{
                                                "id":"6751b98cc1603e117d3c0789d470694a",
                                                "engagement":"1^^Contractual",
                                                "term":"1^^Full Time",
                                                "type":"2^^Consultant",
                                                "relocation":"no",
                                                "payment_type":"yearly",
                                                "currency":"USD",
                                                "negotiable":null,
                                                "salary_start":"15000",
                                                "salary_end":"25000"
                                              },
                                "education":{
                                                "id":"270130ea44dc4350d7e9885542dc97b4",
                                                "institute":"Engineering College",
                                                "passing_year":null,
                                                "year_to":null,
                                                "year_from":null,
                                                "grade_type":null,
                                                "result":null,
                                                "degree_name":"B. Sc.",
                                                "degree":"12^^Bachelors Degree",
                                                "department":"CSE",
                                                "faculty":"CSE",
                                                "city":"",
                                                "state":null,
                                                "country":null,
                                                "specialization":null,
                                                "field_of_study": "100331: Engineering",
                                                "notes":null
                                            }
                    }';

        return json_decode($json);
    }
}