<?php

define("TEST_DIR", realpath(__DIR__));

require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/../app/GearmanKernel.php';
require_once __DIR__ . '/test_helper.php';
use Symfony\Component\Yaml\Parser;

global $config;
$yaml = new Parser();
$config = $yaml->parse(file_get_contents(__DIR__ . '/../app/config/config.yml'));