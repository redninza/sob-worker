<?php

$params = $argv;

$gearman = new \GearmanClient();
$gearman->addServer($params[1], $params[2]);

$gearman->addTaskBackground($params[3], $params[4]);
$gearman->runTasks();