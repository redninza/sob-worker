<?php

use Symfony\Component\Yaml\Parser;

class GearmanKernel
{
    /** @var array */
    protected $conf;

    /** @var Twig_Environment */
    protected $twig;

    /** @var Swift_Mailer */
    protected $mailer;

    /** @var Swift_Plugins_Logger */
    protected $mailerLogger;

    /** @var GearmanWorker */
    protected $worker;

    /** @var \Swift_Preferences */
    protected $mailerPreferences;

    /** @var array */
    protected $taskGroups;

    public function __construct()
    {
        $this->loadConfiguration();
        $this->loadTemplating();
        $this->initializeMailer();
        $this->setupGearman();
        $this->registerWorkers();
    }

    private function loadConfiguration()
    {
        $yaml = new Parser();

        $this->conf = $yaml->parse(file_get_contents(__DIR__ . '/config/config.yml'));
        $this->taskGroups = $yaml->parse(file_get_contents(__DIR__ . '/config/task_groups.yml'));
    }

    private function loadTemplating()
    {
        $twigLoader = new Twig_Loader_Filesystem(__DIR__ . '/../templates');
        //$this->twig = new Twig_Environment($twigLoader, array('cache' => __DIR__ . '/../templates/cache'));
        $this->twig = new Twig_Environment($twigLoader, array());
    }

    private function initializeMailer()
    {
        $host = $this->conf['mail']['host'];
        $user = $this->conf['mail']['username'];
        $pass = $this->conf['mail']['password'];
        $port = $this->conf['mail']['port'];

        $transport = Swift_FailoverTransport::newInstance(array(
            Swift_SmtpTransport::newInstance($host, $port, 'tls')->setUsername($user)->setPassword($pass),
            Swift_NullTransport::newInstance()
        ));

        $this->mailer = Swift_Mailer::newInstance($transport);

        $this->mailerPreferences = Swift_Preferences::getInstance();

        $this->mailerLogger = new Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($this->mailerLogger));
    }

    private function setupGearman()
    {
        $this->worker = new \GearmanWorker();
        $this->worker->addServer($this->conf['gearman']['host'], $this->conf['gearman']['port']);
    }

    private function registerWorkers()
    {
        $dirIterator = new DirectoryIterator(__DIR__ . '/../src/Task');

        foreach ($dirIterator as $file) {

            if ($file->isFile() && $file->getExtension() == 'php') {

                $task = $file->getBasename('.php');
                $eventClass = 'Task\\' . $task;

                if ($eventClass != 'Task\Base') {
                    $workerObject = new $eventClass($this->conf, $this->twig, $this->mailer, $this->mailerLogger, $this->mailerPreferences);
                    $this->worker->addFunction($workerObject->getFunction(), array($workerObject, 'run'));
                }

            }

        }
    }

    public function run()
    {
        while (1) {

            print '[' . date('Y-m-d H:i:s') . '] ' . "Waiting for job...\n";
            $this->worker->work();

            if ($this->worker->returnCode() != GEARMAN_SUCCESS) {
                break;
            }
        }
    }
}