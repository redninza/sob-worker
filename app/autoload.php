<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/symfony/class-loader/Symfony/Component/ClassLoader/UniversalClassLoader.php';
require_once __DIR__ . '/../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
require_once __DIR__ . '/../vendor/swiftmailer/swiftmailer/lib/swift_init.php';
require_once __DIR__ . '/GearmanKernel.php';

use Symfony\Component\ClassLoader\UniversalClassLoader;

$loader = new UniversalClassLoader();
$loader->register();

$src = array(
    'Task' => __DIR__ . '/../src',
    'Model' => __DIR__ . '/../src',
    'Service' => __DIR__ . '/../src'
);

$loader->registerNamespaces($src);

Twig_Autoloader::register();